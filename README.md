# Project

Create a mobile app with one view showing the list of the most recent photos posted on Instagram with the account below, following these guidelines:

- list must show the photos posted by the account provided below
- some photo metadata (title, comments, dates, number of likes…) should be displayed
- list must be built to show any number of photos

## Features implemented

- Recents photos with the account access
- Title / Like / Date / Picture / Place / Profile username / Profile picture
- Share picture
- Like picture
- UI Design
- Scroll to hide navbar
- Zoom on picture like Instagram
- Open website link tapping on picture

