//
//  Image.swift
//  InstagramLike
//
//  Created by Paul Jarysta on 16/12/2019.
//  Copyright © 2019 Paul Jarysta. All rights reserved.
//

import Foundation
import ObjectMapper

// MARK: - Image

struct Image: Mappable {
	
	var low_resolution: LowResolution?
	var thumbnail: Thumbnail?
	var standard_resolution: StandardResolution?
	
	init?(map: Map) {
		
	}
	
	mutating func mapping(map: Map) {
		low_resolution <- map["low_resolution"]
		thumbnail <- map["thumbnail"]
		standard_resolution <- map["standard_resolution"]
	}
}
