//
//  Count.swift
//  InstagramLike
//
//  Created by Paul Jarysta on 15/12/2019.
//  Copyright © 2019 Paul Jarysta. All rights reserved.
//

import Foundation
import ObjectMapper

// MARK: - Counts

struct Counts: Mappable {

	var media: Int?
	var follows: Int?
	var followed_by: Int?
	
	init?(map: Map) {
		
	}
	
	mutating func mapping(map: Map) {
		media <- map["media"]
		follows <- map["follows"]
		followed_by <- map["followed_id"]
	}
	
}
