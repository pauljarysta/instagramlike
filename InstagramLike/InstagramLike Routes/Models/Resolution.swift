//
//  Resolution.swift
//  InstagramLike
//
//  Created by Paul Jarysta on 15/12/2019.
//  Copyright © 2019 Paul Jarysta. All rights reserved.
//

import Foundation
import ObjectMapper

// MARK: - Resolution image

struct LowResolution: Mappable {
	
	var url: String?
	var width: Int?
	var height: Int?
	
	init?(map: Map) {
		
	}
	
	mutating func mapping(map: Map) {
		url <- map["url"]
		width <- map["width"]
		height <- map["height"]
	}
}

struct Thumbnail: Mappable {
	
	var url: String?
	var width: Int?
	var height: Int?
	
	init?(map: Map) {
		
	}
	
	mutating func mapping(map: Map) {
		url <- map["url"]
		width <- map["width"]
		height <- map["height"]
	}
}

struct StandardResolution: Mappable {
	
	var url: String?
	var width: Int?
	var height: Int?
	
	init?(map: Map) {
		
	}
	
	mutating func mapping(map: Map) {
		url <- map["url"]
		width <- map["width"]
		height <- map["height"]
	}
}
