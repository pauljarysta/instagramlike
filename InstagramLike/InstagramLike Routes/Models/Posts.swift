//
//  Posts.swift
//  InstagramLike
//
//  Created by Paul Jarysta on 16/12/2019.
//  Copyright © 2019 Paul Jarysta. All rights reserved.
//

import Foundation
import ObjectMapper

// MARK: - Posts

struct Posts: Mappable {
	
	var id: String?
	var tags: [String]?
	var type: String?
	var location: Location?
	var filter: String?
	var created_time: String?
	var link: String?
	var likes: Like?
	var images: Image?
	var user: UserProfile?
	var caption: Caption?
	
	init?(map: Map) {
		
	}
	
	mutating func mapping(map: Map) {
		id <- map["id"]
		tags <- map["tags"]
		type <- map["type"]
		location <- map["location"]
		filter <- map["filter"]
		created_time <- map["created_time"]
		link <- map["link"]
		likes <- map["likes"]
		images <- map["images"]
		user <- map["user"]
		caption <- map["caption"]
	}
}
