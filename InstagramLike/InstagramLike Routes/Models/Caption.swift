//
//  Caption.swift
//  InstagramLike
//
//  Created by Paul Jarysta on 16/12/2019.
//  Copyright © 2019 Paul Jarysta. All rights reserved.
//

import Foundation
import ObjectMapper

// MARK: - Image

struct Caption: Mappable {
	
	var text: String?
	
	init?(map: Map) {
		
	}
	
	mutating func mapping(map: Map) {
		text <- map["text"]
	}
}
