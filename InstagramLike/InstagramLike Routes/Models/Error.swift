//
//  Error.swift
//  InstagramLike
//
//  Created by Paul Jarysta on 16/12/2019.
//  Copyright © 2019 Paul Jarysta. All rights reserved.
//

import Foundation
import ObjectMapper

// MARK: - Error

struct Error: Mappable {
	
	var message: String?
	
	init?(map: Map) {
		
	}
	
	mutating func mapping(map: Map) {
		message <- map["message"]
	}
}
