//
//  Like.swift
//  InstagramLike
//
//  Created by Paul Jarysta on 15/12/2019.
//  Copyright © 2019 Paul Jarysta. All rights reserved.
//

import Foundation
import ObjectMapper

// MARK: - Like

struct Like: Mappable {
	
	var count: Int?
	
	init?(map: Map) {
		
	}
	
	mutating func mapping(map: Map) {
		count <- map["count"]
	}
}
