//
//  Location.swift
//  InstagramLike
//
//  Created by Paul Jarysta on 16/12/2019.
//  Copyright © 2019 Paul Jarysta. All rights reserved.
//

import Foundation
import ObjectMapper

// MARK: - Location

struct Location: Mappable {
	
	var latitude: Double?
	var longitude: Double?
	var name: String?
	var id: Int?
	
	init?(map: Map) {
		
	}
	
	mutating func mapping(map: Map) {
		latitude <- map["latitude"]
		longitude <- map["longitude"]
		name <- map["name"]
		id <- map["id"]
	}
}
