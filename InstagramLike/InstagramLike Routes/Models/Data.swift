//
//  Data.swift
//  InstagramLike
//
//  Created by Paul Jarysta on 15/12/2019.
//  Copyright © 2019 Paul Jarysta. All rights reserved.
//

import Foundation
import ObjectMapper

// MARK: - DataProfile

struct DataProfile: Mappable {
	var dataProfile: UserProfile?
	
	init?(map: Map) {
		
	}
	
	mutating func mapping(map: Map) {
		dataProfile <- map["data"]
	}
}

// MARK: - DataPosts

struct DataPosts: Mappable {
	var dataPosts: [Posts]?
	
	init?(map: Map) {
		
	}
	
	mutating func mapping(map: Map) {
		dataPosts <- map["data"]
	}
}
