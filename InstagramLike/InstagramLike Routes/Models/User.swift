//
//  User.swift
//  InstagramLike
//
//  Created by Paul Jarysta on 15/12/2019.
//  Copyright © 2019 Paul Jarysta. All rights reserved.
//

import Foundation
import ObjectMapper

// MARK: - UserProfile

struct UserProfile: Mappable {
	
	var id: Int?
	var username: String?
	var fullname: String?
	var profilePicture: String?
	var bio: String?
	var website: String?
	var counts: Counts?
	
	init?(map: Map) {
		
	}
	
	mutating func mapping(map: Map) {
		id <- map["id"]
		username <- map["username"]
		fullname <- map["full_name"]
		profilePicture <- map["profile_picture"]
		bio <- map["bio"]
		website <- map["website"]
		counts <- map["counts"]

	}
}
