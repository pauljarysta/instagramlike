//
//  InstagramLikeRoutes.swift
//  InstagramLike
//
//  Created by Paul Jarysta on 12/12/2019.
//  Copyright © 2019 Paul Jarysta. All rights reserved.
//

import UIKit
import Foundation
import Alamofire

class InstagramLikeRoutes {
    
    var networkErrorDelegate: NetworkErrorDelegate?

	class var sharedInstance: InstagramLikeRoutes {
        struct Static {
            static var instance: InstagramLikeRoutes = InstagramLikeRoutes()
        }
		return Static.instance
    }
    
    let baseUrl: String!
	let accessToken: String!
    	
	init() {
		baseUrl = "https://api.instagram.com/v1"
		accessToken = "5702866500.e4414e3.5eae945c2d364d70b06c1212d8a09362"
	}
		
	func getUrlRequest(path: String) -> URLRequest {
		let url = URL(string: baseUrl+path+"?access_token=\(accessToken!)")
				
		let urlRequest = URLRequest(url: url!)
		
		return urlRequest
	}
	
	func performRequest(request: URLRequestConvertible,
						viewControllerToPresent: UIViewController? = nil,
						fullManualErrorHandling: Bool = false,
						before: (() -> Void)? = nil,
						success: ((_ json: AnyObject?, _ error: NSError?) -> Void)? = nil,
						failure: ((_ json: AnyObject?, _ error: NSError?, _ handledError: Bool) -> Void)? = nil,
						after: ((_ json: AnyObject?, _ error: NSError?) -> Void)? = nil) {
		
		before?()
		
		do {
			let request = try request.asURLRequest()
			
			if let httpMethod = request.httpMethod, let url = request.url {
				print("[\(httpMethod)][\(url)]\n")
			}
			
			if (request.httpBody != nil) {
				print("[HTTPBody][\(NSString(data: request.httpBody!, encoding: String.Encoding.utf8.rawValue)!)]\n")
			}
			
			Alamofire.request(request)
				.responseJSON { (response) in
				
					if let statusCode = response.response?.statusCode {
						print("[Status code][\(statusCode)]")
					}
					
					if fullManualErrorHandling {
						
						if let statusCode = response.response?.statusCode, statusCode >= 200 && statusCode < 300 {
							if let value = response.result.value {
								success!((value as AnyObject), nil)
							}
						} else {
							
							if let value = response.result.value {
								failure!((value as AnyObject?)!, nil, false)
							}
						}
						
					}
					after?(nil, nil)
			}
			
		} catch {
			// Handle error here.
		}
	}
}
