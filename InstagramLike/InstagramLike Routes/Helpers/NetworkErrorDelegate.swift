//
//  NetworkErrorDelegate.swift
//  InstagramLike
//
//  Created by Paul Jarysta on 12/12/2019.
//  Copyright © 2019 Paul Jarysta. All rights reserved.
//

import Foundation
import UIKit

protocol NetworkErrorDelegate {
    var viewController: UIViewController? { get set }

    func somethingWentWrong(viewControllerToPresent: UIViewController?)
    func statusCodeDisconnected(viewControllerToPresent: UIViewController?)
    func handleAdvancedError(errorCode: Int, viewControllerToPresent: UIViewController?)

}
