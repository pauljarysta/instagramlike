//
//  ProfileRoute.swift
//  InstagramLike
//
//  Created by Paul Jarysta on 15/12/2019.
//  Copyright © 2019 Paul Jarysta. All rights reserved.
//

import Foundation
import Alamofire

enum ProfileRoute: URLRequestConvertible {
	
	case ReadProfile
	
	var method: Alamofire.HTTPMethod {
		switch self {
		default:
			return Alamofire.HTTPMethod.get
		}
	}
	
	var path: String {
		switch self {
		case .ReadProfile:
			return "/users/self/"
		}
	}
	
	func asURLRequest() throws -> URLRequest {
		var urlRequest = InstagramLikeRoutes.sharedInstance.getUrlRequest(path: path)
		
		urlRequest.httpMethod = method.rawValue
		
		return urlRequest as URLRequest
	}
}
