//
//  WebViewController.swift
//  InstagramLike
//
//  Created by Paul Jarysta on 16/12/2019.
//  Copyright © 2019 Paul Jarysta. All rights reserved.
//

import UIKit
import WebKit

class WebViewController: UIViewController {

	@IBOutlet weak var webView: WKWebView!
	
	var url: String?
	
	override func viewDidLoad() {
        super.viewDidLoad()
		let link = URL(string: url!)
		let request = URLRequest(url: link!)
		webView.load(request)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
