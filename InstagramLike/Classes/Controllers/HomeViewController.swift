//
//  HomeViewController.swift
//  InstagramLike
//
//  Created by Paul Jarysta on 12/12/2019.
//  Copyright © 2019 Paul Jarysta. All rights reserved.
//

import UIKit
import Kingfisher

class HomeViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
	
	typealias T = PostsRepository
	var repository: T?
	
	// Vars
	var lastContentOffset: CGFloat = 0.0
	
	func configureRepository(repository: T) {
		self.repository = repository
		self.repository?.delegate = self
	}
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
		
		if repository == nil {
			configureRepository(repository: PostsRepository())
		}
		self.repository?.fetch()
    
        setupNavigationBar()
        setupTableView()
        
        let nib = UINib(nibName: "PostTableViewCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "PostTableViewCell")
    }
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		showNavBar()
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		showNavBar()
	}
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

		if  (segue.identifier == "showURL") {
		
			let destinationVC = segue.destination as? WebViewController
			let indexRow = tableView.indexPathForSelectedRow?.row
			
			if let items = self.repository?.itemsPosts {
				if let posts = items.dataPosts {
					if let link = posts[indexRow!].link {
						destinationVC!.url = link
					}
				}
			}
		}
	}
	
	// MARK: - Style
	func setupNavigationBar() {
		navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "send").withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: nil)
	}
	
	func setupTableView() {
		tableView.separatorStyle = .none
		tableView.estimatedRowHeight = 580.0
		tableView.rowHeight = UITableView.automaticDimension
	}
	
	func updateNavBar(y: CGFloat) {
		UIView.animate(withDuration: 0.3, delay: 0.0, options: .curveEaseInOut, animations: {
			self.navigationController?.navigationBar.frame = CGRect(
				x: (self.navigationController?.navigationBar.frame.origin.x)!,
				y: y,
				width: (self.navigationController?.navigationBar.frame.size.width)!,
				height: (self.navigationController?.navigationBar.frame.size.height)!)
		})
	}
	
	func hideNavBar() {
		if (Device.IS_IPHONE_X) {
			self.updateNavBar(y: 0)
		} else {
			self.updateNavBar(y: 20 - (20 + 44))
		}
	}
	
	func showNavBar() {
		if (Device.IS_IPHONE_X) {
			self.updateNavBar(y: 44)
		} else {
			self.updateNavBar(y: 20)
		}
	}
	
	func scrollViewDidScroll(_ scrollView: UIScrollView) {
		if lastContentOffset < 0.0 {
			
		} else if lastContentOffset > scrollView.contentOffset.y  {
			showNavBar()
			
		} else if lastContentOffset < scrollView.contentOffset.y {
			hideNavBar()
		}
		lastContentOffset = scrollView.contentOffset.y
	}
	
	
	// MARK: - Functions

	@objc func shareTapped(_ sender: UIButton) {
		
		if let items = self.repository?.itemsPosts {
			if let posts = items.dataPosts {
				if let images = posts[sender.tag].images {
					if let resolution = images.standard_resolution {
						if let imageUrl = resolution.url {
							let urlPicturePost = URL(string: imageUrl)
							let items = [urlPicturePost]
							let ac = UIActivityViewController(activityItems: items as [Any], applicationActivities: nil)
							present(ac, animated: true)
						}
					}
				}
			}
		}
	}
}


// MARK: - UITableViewDatasource

extension HomeViewController: UITableViewDataSource {
	
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		if let items = self.repository?.itemsPosts {
			if let posts = items.dataPosts {
				return posts.count
			}
		}
		return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:PostTableViewCell = tableView.dequeueReusableCell(withIdentifier: "PostTableViewCell", for: indexPath) as! PostTableViewCell
		
		cell.indexPathRow = indexPath.row
		
		if let items = self.repository?.itemsPosts {
			if let posts = items.dataPosts {
				cell.fillCellWith(row: posts[indexPath.row])
			}
		}
		
		cell.shareButton.addTarget(self, action: #selector(shareTapped(_:)), for: .touchUpInside)
		
		return cell
	}
	
}

// MARK: - UITableViewDelegate

extension HomeViewController: UITableViewDelegate {
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		performSegue(withIdentifier: "showURL", sender: nil)
	}

}

extension HomeViewController: PostsRepositoryDelegate {
	
	func didFetch(error: Error?) {
		
		if let error = error {
			
			if let message = error.message {
				Alert.start.view(viewController: self, style: .alert, alertTitle: "Error", alertMessage: "\(message)")
			}
			
		} else {
			// guard let items = self.repository?.itemsPosts else { return }
			tableView.reloadData()
		}
	}
	
	
}

