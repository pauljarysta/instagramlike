//
//  PostsRepository.swift
//  InstagramLike
//
//  Created by Paul Jarysta on 16/12/2019.
//  Copyright © 2019 Paul Jarysta. All rights reserved.
//

import Foundation
import ObjectMapper
import SwiftyJSON

protocol PostsRepositoryDelegate {
	func didFetch(error: Error?)
}

class PostsRepository {
	
	var delegate : PostsRepositoryDelegate?
	
	var itemsPosts: DataPosts?
	
	func fetch() {
		
		InstagramLikeRoutes.sharedInstance.performRequest(
			request: PostsRoute.ReadPosts,
			fullManualErrorHandling: true,
			before: { () -> () in
				
		}, success: { (json: AnyObject?, error: NSError?) -> (Void) in
			
			if let posts = Mapper<DataPosts>().map(JSONObject: json) {
				self.didFetch(item: posts, error: nil)
			}
			
		}, failure: { (json: AnyObject?, error: NSError?, handledError: Bool) -> (Void) in
			
		}, after: { (json: AnyObject?, error: NSError?) -> (Void) in
			
		})
	}
	
	func didFetch(item: DataPosts, error: Error?) {
		if error != nil {
			self.delegate?.didFetch(error: error)
		} else {
			self.itemsPosts = item
			self.delegate?.didFetch(error: nil)
		}
		
	}
}
