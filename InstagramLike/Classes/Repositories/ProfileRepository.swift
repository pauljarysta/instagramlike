//
//  ProfileRepository.swift
//  InstagramLike
//
//  Created by Paul Jarysta on 15/12/2019.
//  Copyright © 2019 Paul Jarysta. All rights reserved.
//

import Foundation
import ObjectMapper
import SwiftyJSON

protocol ProfileRepositoryDelegate {
	func didFetch(error: Error?)
}

class ProfileRepository {
	
	var delegate : ProfileRepositoryDelegate?
	
	var itemsProfile: DataProfile?
	
	func fetch() {
		
		InstagramLikeRoutes.sharedInstance.performRequest(
			request: ProfileRoute.ReadProfile,
			fullManualErrorHandling: true,
			before: { () -> () in
				
		}, success: { (json: AnyObject?, error: NSError?) -> (Void) in
		
			if let profile = Mapper<DataProfile>().map(JSONObject: json) {
				self.didFetch(item: profile, error: nil)
			}
			
		}, failure: { (json: AnyObject?, error: NSError?, handledError: Bool) -> (Void) in
			
		}, after: { (json: AnyObject?, error: NSError?) -> (Void) in
			
		})
	}
	
	func didFetch(item: DataProfile, error: Error?) {
		if error != nil {
			self.delegate?.didFetch(error: error)
		} else {
			self.itemsProfile = item
			self.delegate?.didFetch(error: nil)
		}

	}
}
