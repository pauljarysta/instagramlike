//
//  AbstractRepository.swift
//  InstagramLike
//
//  Created by Paul Jarysta on 15/12/2019.
//  Copyright © 2019 Paul Jarysta. All rights reserved.
//

import Foundation

protocol AbstractRepository {
	associatedtype T
	var repository : T? { get set }
	
	func configureRepository(repository: T)
}
