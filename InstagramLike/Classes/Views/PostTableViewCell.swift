//
//  PostTableViewCell.swift
//  InstagramLike
//
//  Created by Paul Jarysta on 13/12/2019.
//  Copyright © 2019 Paul Jarysta. All rights reserved.
//

import UIKit

class PostTableViewCell: UITableViewCell {
    
	// View
    @IBOutlet weak var imageProfile: UIImageView!
    @IBOutlet weak var usernameProfile: UILabel!
	@IBOutlet weak var placePost: UILabel!
	
    @IBOutlet weak var picturePost: UIImageView!
    
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var commentButton: UIButton!
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var saveButton: UIButton!
    
    @IBOutlet weak var nbLike: UILabel!
    
    @IBOutlet weak var usernameProfileDesc: UILabel!
    @IBOutlet weak var desc: UITextView!
	@IBOutlet weak var postDate: UILabel!
	
	// Vars
	var isZooming = false
	var originalImageCenter: CGPoint?
	var indexPathRow: Int?
	
	var saveLikeCount = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initStyle()
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    required public init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    func initStyle() {
        selectionStyle = .none
        clipsToBounds = false
        backgroundColor = .clear
        setupView()
    }
        
    func setupView() {

        guard let _imageProfile = imageProfile else { return }
        _imageProfile.layer.cornerRadius = _imageProfile.frame.size.width / 2
        
        guard let pictureView = picturePost else { return }
		pictureView.isUserInteractionEnabled = true
        pictureView.translatesAutoresizingMaskIntoConstraints = false
        pictureView.contentMode = .scaleAspectFill
        
        likeButton.setImage(UIImage(named: "like-off"), for: .normal)

        let pinch = UIPinchGestureRecognizer(target: self, action: #selector(self.pinch(sender:)))
		pinch.delegate = self
        pictureView.addGestureRecognizer(pinch)
		
		let pan = UIPanGestureRecognizer(target: self, action: #selector(self.pan(sender:)))
		pan.delegate = self
		pictureView.addGestureRecognizer(pan)
    }
	
	func fillCellWith(row: Posts) {
		
		if let userProfile = row.user {
		
			if let profilePicture = userProfile.profilePicture {
				let urlPictureProfile = URL(string: profilePicture)
				imageProfile.kf.setImage(with: urlPictureProfile)
			}
			
			if let username = userProfile.username  {
				usernameProfile.text = "\(username)"
				usernameProfileDesc.text = "\(username)"
			} else {
				usernameProfile.text = ""
				usernameProfileDesc.text = ""
			}
			
		}

		if let location = row.location {
			if let name = location.name {
				placePost.text = "\(name)"
			} else {
				placePost.text = ""
			}
		} else {
			placePost.text = ""
		}

		if let like = row.likes {
			if let likeCount = like.count {
				saveLikeCount = like.count!
				nbLike.text = "\(likeCount) Like"
			} else {
				nbLike.text = "0 Like"
			}
		} else {
			nbLike.text = "0 Like"
		}
		
		if let images = row.images {
			if let resolution = images.standard_resolution {
				if let imageUrl = resolution.url {
					let urlPicturePost = URL(string: imageUrl)
					picturePost.kf.setImage(with: urlPicturePost)
				}
			}
		}
		
		if let caption = row.caption {
			if let description = caption.text {
				desc.text = description
			}
		} else {
			desc.text = ""
		}
		
		if let created_time = row.created_time {
			let date = Date(timeIntervalSince1970: Double(created_time)!)
			let dateFormatter = DateFormatter()
			dateFormatter.timeStyle = DateFormatter.Style.medium //Set time style
			dateFormatter.dateStyle = DateFormatter.Style.medium //Set date style
			dateFormatter.timeZone = .current
			postDate.text = "\(dateFormatter.string(from: date))"
		}
	}
    
	
	override func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
		return true
	}
    
    // MARK: - Functions
	
	@objc func pan(sender: UIPanGestureRecognizer) {
		if (self.isZooming && sender.state == .began) {
			self.originalImageCenter = sender.view?.center
		} else if (self.isZooming && sender.state == .changed) {
			let translation = sender.translation(in: self)
			if let view = sender.view {
				view.center = CGPoint(x:view.center.x + translation.x,
									  y:view.center.y + translation.y)
			}
			sender.setTranslation(CGPoint.zero, in: self.picturePost.superview)
		}
	}
    
	@objc func pinch(sender: UIPinchGestureRecognizer) {
		
		if (sender.state == .began) {
			let currentScale = picturePost.frame.size.width / picturePost.bounds.size.width
			let transformScale = currentScale * sender.scale

			if (transformScale > 1) {
				isZooming = true
			}
		} else if (sender.state == .changed) {
		
			guard let view = sender.view else { return }
			
			let pinchCenter = CGPoint(x: sender.location(in: view).x - view.bounds.midX,
									  y: sender.location(in: view).y - view.bounds.midY)
			
			let transform = view.transform.translatedBy(x: pinchCenter.x, y: pinchCenter.y).scaledBy(x: sender.scale, y: sender.scale).translatedBy(x: -pinchCenter.x, y: -pinchCenter.y)
			
			let currentScale = picturePost.frame.size.width / picturePost.bounds.size.width
			
			var transformScale = currentScale * sender.scale
			
			if (transformScale < 1) {
				transformScale = 1
				let transform = CGAffineTransform(scaleX: transformScale, y: transformScale)
				picturePost.transform = transform
				sender.scale = 1
			} else {
				view.transform = transform
				sender.scale = 1
			}
			
		} else if (sender.state == .ended || sender.state == .failed || sender.state == .cancelled) {
			guard let center = self.originalImageCenter else { return }
			UIView.animate(withDuration: 0.3, animations: {
				self.picturePost.transform = CGAffineTransform.identity
				self.picturePost.center = center
			}, completion: { _ in
				self.isZooming = false
			})
		}
		
	}
    
    // MARK: - IBAction
    
    @IBAction func doLike(_ sender: UIButton) {
				        
		if (sender.isSelected == false) {
			sender.setImage(UIImage(named: "like-on") , for: .normal)
			sender.isSelected = true
			saveLikeCount += 1
			nbLike.text = "\(saveLikeCount) Like"
		} else {
			sender.setImage(UIImage(named: "like-off") , for: .normal)
			sender.isSelected = false
			saveLikeCount -= 1
			nbLike.text = "\(saveLikeCount) Like"
		}
		
    }
    
    @IBAction func doBookmark(_ sender: UIButton) {
        if (sender.isSelected == false) {
            sender.setImage(UIImage(named: "ribbon-on") , for: .normal)
            sender.isSelected = true
        } else {
            sender.setImage(UIImage(named: "ribbon") , for: .normal)
            sender.isSelected = false
        }
        
    }
}
