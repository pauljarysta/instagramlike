//
//  Alert.swift
//  InstagramLike
//
//  Created by Paul Jarysta on 16/12/2019.
//  Copyright © 2019 Paul Jarysta. All rights reserved.
//

import UIKit
import Foundation

class Alert {
	
	static let start: Alert = {
		let instance = Alert()
		return instance
	}()
	
	func info(viewController: UIViewController, alertTitle: String, alertMessage: String, cancelTitle: String, completion: (() -> Void)? = nil) {
		
		let alert = UIAlertController(title: alertTitle,
									  message: alertMessage,
									  preferredStyle: .alert)
		
		
		alert.addAction(UIAlertAction(title: cancelTitle,style: .default) { action in
			// viewController.dismiss(animated: true, completion: nil)
		})
		
		viewController.present(alert, animated: true, completion: nil)
	}
	
	func view(viewController: UIViewController, style: UIAlertController.Style, alertTitle: String, alertMessage: String) {
		
	}
}
